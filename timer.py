from kivy.app import App
from kivy.config import Config

Config.set('graphics', 'width', '352')
Config.set('graphics', 'height', '352')
Config.set('kivy','window_icon','daystart.png')
Config.set('input', 'mouse', 'mouse,disable_multitouch')

from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.properties import StringProperty
from kivy.uix.popup import Popup
import datetime as dt
import os

os.makedirs('./logs', exist_ok=True)

class Main(Widget):
	
	started = False
	onBreak = False
	onTask = False

	start = dt.datetime.now()
	breaks = dt.datetime(1,1,1)
	end = start+dt.timedelta(hours=8,minutes=30)
	timer = dt.datetime(1,1,1)
	check = dt.datetime(1,1,1)
	taskname = ''
	
	strStartTime = StringProperty(start.strftime('%H:%M:%S'))
	strBreaks = StringProperty(str(breaks.time()))
	strEnd = StringProperty(end.strftime('%H:%M:%S'))
	strTimer = StringProperty(str(timer.time()))
	strCheck = StringProperty(str(check.time()))

	def __init__(self, **kwargs):
		super(Main, self).__init__(**kwargs)
		Clock.schedule_interval(self.update,1)

	def rvInsert(self,string):
		self.ids.rv.data.insert(0,{'text': dt.datetime.now().strftime('%H:%M:%S')+' - '+string})

	def update(self,delta):
		def increment(time,string):
			return time+dt.timedelta(seconds=1),time.strftime('%H:%M:%S')
		
		if self.started == True:
			if self.onBreak == False:
				self.timer,self.strTimer = increment(self.timer,self.strTimer)
				if self.onTask == True:
					self.check,self.strCheck = increment(self.check,self.strCheck)
			else:
				self.breaks,self.strBreaks = increment(self.breaks,self.strBreaks)
				if self.breaks.minute >= 30:
					self.end,self.strEnd = increment(self.end,self.strEnd)
		else:
			self.start = dt.datetime.now()
			self.strStartTime = self.start.strftime('%H:%M:%S')
			self.end = self.start+dt.timedelta(hours=8,minutes=30)
			self.strEnd = self.end.strftime('%H:%M:%S')

	def day(self):
		if self.started == False:
			self.started = True
			self.rvInsert('Day Started')
			self.ids.dayImg.source = 'dayend.png'
			self.popup()
		else:
			self.rvInsert('Day Ended')
			with open('./logs/'+str(dt.datetime.now().date())+'.txt','w') as file:
				file.write('Day Length: '+str(dt.datetime.now()-self.start).split('.')[0]+'\n')
				file.write('Break Time: '+self.breaks.strftime('%H:%M:%S')+'\n')

				for line in self.ids.rv.data:
					file.write(line['text']+'\n')
			
			self.ids.rv.data = []
			self.start = dt.datetime.now()
			self.breaks = dt.datetime(1,1,1)
			self.end = self.start+dt.timedelta(hours=8,minutes=30)
			self.timer = dt.datetime(1,1,1)
			self.check = dt.datetime(1,1,1)
			self.strBreaks = str(self.breaks.time())
			self.strCheck = str(self.check.time())
			self.strTimer = str(self.timer.time())
			self.ids.dayImg.source = 'daystart.png'
			self.started = False

	def popup(self):
		if self.started == True:
			p = CheckPopup(self)
			p.open()
	
	def breaktime(self):
		if self.started == True:
			if self.onBreak == False:
				self.rvInsert('Break started')
				self.ids.breakImg.source = 'daystart.png'
			else:
				self.rvInsert('Break ended')
				self.ids.breakImg.source = 'startBreak.png'
			
			self.onBreak = not self.onBreak


class CheckPopup(Popup):
	def __init__(self,my_widget,**kwargs):
		super(CheckPopup,self).__init__(**kwargs)
		self.root = my_widget
		Clock.schedule_once(self.focus,0.2)
	
	def focus(self,delta):
		self.ids.input.focus = True

	def checkpoint(self,text):
		if self.root.onTask == True:
			self.root.rvInsert('Task '+self.root.taskname+' complete in '+str(self.root.check.time()))
		self.root.onTask = True		
		self.root.check = dt.datetime(1,1,1)
		self.root.rvInsert('New Task stated: '+text)
		self.root.taskname = text


class Timer(App):
	def build(self):
		return Main()


if __name__ == '__main__':
	Timer().run()
